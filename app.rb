# coding: utf-8

while true
    print "Convert to binary or UTF-8? (b/u/stop): "
    
    case gets.chomp
    when "b" then
        print "Input in UTF-8: "
        x = gets.chomp
        x = x.unpack('B*')
        puts x

    when "u" then
        print "Input in binary: "
        y = gets.chomp
        y = [y].pack('B*')
        puts y

    when "stop"
        break

    else
        puts "wrong input"
    end
end